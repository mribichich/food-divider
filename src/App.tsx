import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Slider from '@material-ui/core/Slider';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React, { useState } from 'react';

const lunchPct = 0.63;
const dinnerPct = 0.37;

const marks = [0, 1, 2, 3, 4, 5, 6, 7].map(m => ({ label: m, value: m }));

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    mainPaper: {
      padding: theme.spacing(4),
    },
    main: { display: 'flex', flexDirection: 'column' },
  })
);

const App: React.FC = () => {
  const classes = useStyles();
  const [lunch, setLunch] = useState(4);
  const [dinner, setDinner] = useState(4);
  const [recepient, setRecepient] = useState(185);
  const [recepientPlusFood, setRecepientPlusFood] = useState(0);

  function handleLunchChange(event: React.ChangeEvent<{}>, value: number | number[]) {
    setLunch(value as number);
  }

  function handleDinnerChange(event: React.ChangeEvent<{}>, value: number | number[]) {
    setDinner(value as number);
  }

  function handleRecepientChange(event: React.ChangeEvent<HTMLInputElement>) {
    setRecepient(event.target.value as any);
  }

  function handleRecepientPlusFoodChange(event: React.ChangeEvent<HTMLInputElement>) {
    setRecepientPlusFood(event.target.value as any);
  }

  const foodResult = recepientPlusFood - recepient;

  const ratio = foodResult / (lunchPct * lunch + dinnerPct * dinner);

  const lunchResult = Math.round(ratio * lunchPct);
  const dinnerResult = Math.round(ratio * dinnerPct);

  return (
    <Container maxWidth="sm">
      <h1>Food Divider</h1>

      <Paper className={classes.mainPaper}>
        <div className={classes.main}>
          <Typography id="discrete-slider" gutterBottom>
            Almuerzo
          </Typography>
          <Slider
            aria-labelledby="discrete-slider"
            valueLabelDisplay="auto"
            step={1}
            marks={marks}
            min={0}
            max={7}
            value={lunch}
            onChange={handleLunchChange}
          />

          <br />
          <br />

          <Typography id="discrete-slider" gutterBottom>
            Cena
          </Typography>
          <Slider
            aria-labelledby="discrete-slider"
            valueLabelDisplay="auto"
            step={1}
            marks={marks}
            min={0}
            max={7}
            value={dinner}
            onChange={handleDinnerChange}
          />

          <br />
          <br />

          <TextField
            label="Recipiente"
            variant="outlined"
            margin="normal"
            type="number"
            value={recepient}
            onChange={handleRecepientChange}
          />
          <TextField
            label="Recipiente + Comida"
            variant="outlined"
            margin="normal"
            type="number"
            autoFocus
            value={recepientPlusFood || ''}
            onChange={handleRecepientPlusFoodChange}
          />

          <br />
          <br />

          {Boolean(recepientPlusFood) && (
            <div>
              <div>Comida: {foodResult}</div>
              <div>Almuerzo: {lunchResult}</div>
              <div>Cena: {dinnerResult}</div>
            </div>
          )}
        </div>
      </Paper>
    </Container>
  );
};

export default App;
